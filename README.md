# Pom One Apple 1 Emulator



## About

Original Pom1 Source: Verhille Arnaud<br>
http://www.gistlabs.net/soft/pom1/pom1.html

Original Android Pom1 Source: John D. Corrado<br>
https://pom1.sourceforge.net/

Pom One Emulator Source: Brian Ouellette

## Purpose

Pom One is software designed to simulate the hardware of an Apple 1 Computer.


## Getting Started

When you first start the app, you will see a backslash with a blinking cursor under it. This means you are in the Rom Monitor. A simple program to enter bytes into addresses.

To enter a byte into an address you would use a command like 
     
    1000: a9 41 20 ef ff
    
This will enter five bytes starting at address 1000

You can view the bytes with a command like 

    1000.1004

This will list the bytes starting at address 1000

The bytes entered at address is a small program to display the letter A on the screen.

To run the program, you can use the command 1000R. This means you want to Run starting at address 1000.

You shuld see a new line show that says 1000: A9A

This means that we are showing the first byte on address 1000 and then the program runs. We see the A directly after.

To reset and bring us back to the Rom Monitor, use the menu to perform a Reset.

<h1>Menu</h1>
<ul>
    <li>In the menu, we have a few options:</li>
    <li>Show/Hide Keyboard. This will toggle the onscreen keyboard. Allows for more screen when using a tablet in landscape and hardware keyboard.</li>
    <li>Reset: This is a soft reset. No memory is cleared when press and we return back to the Rom Monitor.</li>
    <li>Hard Reset: This is a hard reset. It is like powering off and on. All memory is cleared in this operation.</li>
    <li>Load Memory: This will allow us to open a file to load memory into the emulator. Please see Load Memory Section for more info.</li>
    <li>Save Memory: This will allow us to save to a file memory from the emulator. Please see Save Memory Section for more info.</li>
    <li>About: A simple about windows</li>
    <li>Quit: Shuts down the emulator and returns back to Android.</li>
</ul>

<h1>Load Memory</h1>
<ul>
    <li>We have several ways we can load memory into the emulator:</li>
    <li>Binary File, Ascii Memory, Ascii Text</li>
    <li>A Binary File is all the bytes in binary form. You will need to specify what the starting address will be when loading.</li>
    <li>An Ascii Memory file is a text document with the addresses and bytes listed. You can use Simulate Keyboard for entry but without is faster.</li>
    <li>An Ascii Text file is mostly useful for BASIC listings. You will need to Simulate Keyboard Input to use these.</li>
</ul>

<h1>Save Memory</h1>
<ul>
    <li>We have two ways we can save memory from the emulator:</li>
    <li>Binary File, Ascii Memory</li>
    <li>A Binary File is all the bytes in binary form.</li>
    <li>An Ascii Memory file is a text document with the addresses and bytes listed.</li>
    <li>In both cases, you need a starting address and ending address in HEX format.</li>
</ul>

<h1>BASIC Programming</h1>
<ul>
    <li>Pom One Emulator comes with BASIC preloaded into memory.</li>
    <li>To start the BASIC program, type E000R to load into BASIC</li>
    <li>When you do, you should see a ">" symbol to indicate you have enter into BASIC</li>
    <li>You can test by typing PRINT 2+2 to see the output of 4 below it.</li>
</ul>
